package nastek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JornadaApplication {

    /*@Bean
    public WebSecurityConfigurerAdapter webSecurityConfigurerAdapter() {
        return new ApplicationSecurity();
    }*/

    public static void main(String[] args) {
        SpringApplication.run(JornadaApplication.class, args);
    }
}
