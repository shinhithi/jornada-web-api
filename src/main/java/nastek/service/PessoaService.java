package nastek.service;

import nastek.domain.Pessoa;

import java.util.List;

/**
 * Created by alessandro.yamada on 21/10/2015.
 */
public interface PessoaService {

    Pessoa findByNome(String nome);

    List<Pessoa> findAll();

    Pessoa save(Pessoa pessoa);
}
