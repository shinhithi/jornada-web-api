package nastek.service;


import jersey.repackaged.com.google.common.collect.Lists;
import nastek.domain.Pessoa;
import nastek.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component("pessoaService")
public class PessoaServiceImpl implements PessoaService {

    private final PessoaRepository pessoaRepository;

    @Autowired
    public PessoaServiceImpl(PessoaRepository pessoaRepository) {
        this.pessoaRepository = pessoaRepository;
    }

    @Override
    public Pessoa findByNome(String nome) {
        return pessoaRepository.findByNome(nome);
    }

    @Override
    public List<Pessoa> findAll() {
        return Lists.newArrayList(pessoaRepository.findAll());
    }

    @Override
    @Transactional
    public Pessoa save(Pessoa pessoa) {
        return pessoaRepository.save(pessoa);
    }
}
