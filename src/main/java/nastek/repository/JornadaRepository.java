package nastek.repository;

import nastek.domain.Jornada;
import org.springframework.data.repository.Repository;

public interface JornadaRepository extends Repository<Jornada, Long> {
}
