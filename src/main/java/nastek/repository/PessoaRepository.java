package nastek.repository;

import nastek.domain.Pessoa;
import org.springframework.data.repository.CrudRepository;

public interface PessoaRepository extends CrudRepository<Pessoa, Long> {

    Pessoa findByNome(String nome);

}
