package nastek.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;

@Entity
public class Pessoa extends AbstractPersistable<Long> {

    private static final long serialVersionUID = 1L;

    private String nome;

    private String avatar;

    private String conteudo;

    public String getNome() {
        return nome;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getConteudo() {
        return conteudo;
    }
}
