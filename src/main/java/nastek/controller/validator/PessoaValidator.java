package nastek.controller.validator;

import nastek.domain.Pessoa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.*;

@Component
public class PessoaValidator implements Validator {

    public PessoaValidator() {
    }

    @Override
    public boolean supports(Class clazz) {
        return Pessoa.class.isAssignableFrom(clazz);
    }
    @Override
    public void validate(Object target, Errors errors) {

    }
}