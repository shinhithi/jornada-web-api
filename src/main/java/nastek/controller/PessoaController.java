package nastek.controller;

import nastek.controller.validator.PessoaValidator;
import nastek.domain.Pessoa;
import nastek.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pessoas")
public class PessoaController {

    private final PessoaService pessoaService;
    private final PessoaValidator validator;

    @Autowired
    public PessoaController(PessoaService pessoaService, PessoaValidator validator) {
        this.pessoaService = pessoaService;
        this.validator = validator;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(validator);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Pessoa> list() {
        return pessoaService.findAll();
    }

    @RequestMapping(path = "/{nome}", method = RequestMethod.GET)
    public Pessoa search(@PathVariable String nome) {
        return pessoaService.findByNome(nome);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Pessoa create(@RequestBody Pessoa pessoa) {
        return pessoaService.save(pessoa);
    }

}
